//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');
const WebDiscoveryService = require('../../../../../main/node/infrastructure/service/discovery/web.discovery.service');

describe('WebDiscoveryService', () => {

  let testObj = new WebDiscoveryService();

  it('configure instance based on the provided target', async () => {
    testObj.configure({ target: 'http://httpbin.org' });
    // act
    let instances = await testObj.discover();
    // test
    assert.equal(instances.length, 1);
  });

  it('thorw error if target is not defined', () => {
    try {
      testObj.configure({});
      assert.fail('expected exception to be raised');
    } catch (e) {
      assert.equal(e.message, 'target parameter must be provided for web backend definition');
    }
  });
});