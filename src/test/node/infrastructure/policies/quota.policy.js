//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */
/* global clearInterval */

const assert = require('assert');
const QuotaPolicy = require('../../../../main/node/infrastructure/policies/quota.policy');

describe('DockerDiscoveryService', () => {

  let testObj = new QuotaPolicy();

  it('allows request inside the assigned window', () => {
    testObj.configure({ resetInerval: 10000, window: 1000, allow: 2 });
    // execute
    let actual = testObj.apply({ headers: {}, connection: { remoteAddress: '127.0.0.1' } });
    // assert
    assert.equal(actual, true);
    clearInterval(testObj.cleanUpJob);
  });

  it('blocks request exceeding allowed inside the assigned window', () => {
    testObj.configure({ resetInerval: 10000, window: 1000, allow: 3 });
    for (let i = 0; i < 3; i++) {
      // execute
      let actual = testObj.apply({ headers: {}, connection: { remoteAddress: '127.0.0.1' } });
      // assert
      i < 2 ? assert.equal(actual, true) : assert.equal(actual, false);
    }
    clearInterval(testObj.cleanUpJob);
  });
});