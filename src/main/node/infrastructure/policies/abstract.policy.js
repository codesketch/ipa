//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const Component = require('dino-core').Component;

class AbstractPolicy extends Component {

  /**
   * Execute the policy logic and allow integration with express 
   * @param {Any} req the client request
   * @param {Any} res the client response
   * @param {Function} next a callback invoked when the middleware terminates successfully
   * 
   * @public
   */
  middleware(req, res, next) {
    this.apply(req) ? next() : this.onDeny(res);
  }

  /**
   * Apply the requested policy
   * @param {Any} req the incoming request
   * @returns {Boolean} true if the policy once applied allow access to the API, false otherwise
   * 
   * @protected
   * @abstract
   */
  apply(req) { // eslint-disable-line no-unused-vars
    throw new Error('not implemented');
  }

  /**
   * Allow to configure this policy
   * @param {Any} configuration the policy configuration
   * 
   * @public
   * @abstract
   */
  configure(configuration) { // eslint-disable-line no-unused-vars
    throw new Error('not implemented');
  }

  /**
   * Execute logic that will signal that the policy has denied the client request
   * @param {Any} res the client response
   * 
   * @abstract
   * @protected
   */
  onDeny(res) { // eslint-disable-line no-unused-vars
    throw new Error('not implemented');
  }
}

/**
 * Define the schelethon of a policy
 * @typedef AbstractPolicy
 * @abstract
 */
module.exports = AbstractPolicy;