//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const passport = require('passport');
const FacebookTokenStrategy = require('passport-facebook-token');

const Profile = require('../profile');
const AbstractStrategy = require('../abstract.strategy');

class FacebookStrategy extends AbstractStrategy {

  /**
  * @extends AbstractStrategy#doActivate()
  */
  doActivate(authority) {
    passport.use(this.generateStrategyName('facebook'), new FacebookTokenStrategy(this.getStrategyConfiguration('facebook-token'),
      function (accessToken, refreshToken, profile, done) {
        // verify user is active with facebook id
        return this.authorize(authority, Profile.create(profile.id, 'facebook'), done);
      }
    ));
  }

  /**
   * @extends AbstractStrategy#authenticator();
   */
  authenticator() {
    return passport.authenticate(this.generateStrategyName('facebook'));
  }

  static scope() {
    return 'transient';
  }
}

/**
 * @typedef FacebookStrategy
 */
module.exports = FacebookStrategy;