<!---
    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
 
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
 
        http://www.apache.org/licenses/LICENSE-2.0
 
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->

# IPA

A nodejs API Gateway based on Swagger 2.0, OpenAPI 3.0 API definition configuration.

IPA extends OpenAPI 3.0 specification adding custom definitions that allows to define how the API Gateway is expected to work.

# Features
* **Swagger 2.0/OpenAPI 3.0** based definition

* **Auhentication**, IPA uses [passport](http://www.passportjs.org/) to provide authentication for your APIs, the standard [passport](http://www.passportjs.org/) behaviour is extended allowing you to define your identity validator which integrating with your user/account APIs will allow to determine if a request should be allowed access to your resources.

* **Dynamic Docker Service Discovery**, that is if IPA is deployed on a docker swarm it will dynamically discover and load-balances request to multiple docker services. The load balance implemented is pure round robin.

* **Spike Arest**, spike arrest policy helps protecting the backend against spikes of traffic or Denial of Service attacks, it should not be used to control or limit the number of requests that are received by the backend in a specific time window, for this a quota policy should be used. Configuration parameters for the policy are:

   * **interval**, the interval at which the spike arrest shold be reset expressed in ms
   * **allow**, the number of requests that are allowed inside the window interval

  When the policy is triggered and the requests is denied a 429 HTTP response code is returned with message "Too Many Requests".

  The spike window is calcuated as **(interval/1000)/allow** this allows to segregate requests in spike windows of a specific duration, i.e if interval is set to 60000 (one minute)and allow is set to 30 (30 allow request each minute) the spike window duration will be 2 seconds meaning that only one request is allowed to the backend every 2 seconds.

  The policy will count requests based on the client IP Address. 

* **Quota**, quota policy helps limit the amount of requests that a client can make to the backend over a specific amount of time, it should not be used to protect the backend against spikes. Configuration parameters for the policy are:

   *  **resetInterval**, the interval at which the quotas shold be reset expressed in ms
   *  **window**, the time window the quota should be defined for expressed in ms
   *  **allow**, the number of requests that are allowed inside the window interval

  When the policy is triggered and the requests is denied a 429 HTTP response code is returned with message "Quota Exceeded".

  The policy will count requests based on the client IP Address.

* **Cache**, add caching functionalities for an API, the implementation is based on [apicache](https://github.com/kwhitley/apicache) module. Configuration parameters are:
  * **ttl** the time the response from the API will be kept in the cache

## Supported Authentication
* **Facebook Token** see [passport-facebook-token](https://www.npmjs.com/package/passport-facebook-token)
* **Google ID Token** see [passport-google-idtoken](https://www.npmjs.com/package/passport-google-idtoken)
* **Api Key** see [passport-headerapikey](http://www.passportjs.org/packages/passport-headerapikey/)

## Roadmap
* ~~Support for Docker~~
* Support for Kubernetes
* Request and Response API Validation
* ~~Configuration based API Key validation~~
* API Statistics
* ~~API inspection UI~~
* ~~Release as a container~~